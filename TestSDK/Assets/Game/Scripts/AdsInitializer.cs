using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class AdsInitializer : MonoBehaviour
{
    public string YOUR_APP_KEY = "1b1b9f6d5";

    public TextMeshProUGUI m_RewardText;
    public TextMeshProUGUI m_IntertitialText;
    public GameObject m_obj;
    // Start is called before the first frame update
    private void Awake()
    {
        IronSource.Agent.init(YOUR_APP_KEY);
        //
        StartCoroutine(WaitLoadInit());
    }

    void Start()
    {
        //IronSource.Agent.init(YOUR_APP_KEY);
        IronSource.Agent.init(YOUR_APP_KEY, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.INTERSTITIAL, IronSourceAdUnits.OFFERWALL, IronSourceAdUnits.BANNER);
        //
        IronSourceEvents.onSdkInitializationCompletedEvent += SdkInitializationCompletedEvent;
        //
        Loadbanner();
        LoadInterstitial();
    }
    private void Update()
    {
        //
        m_RewardText.text = IronSource.Agent.isRewardedVideoAvailable().ToString();
        m_IntertitialText.text = IronSource.Agent.isInterstitialReady().ToString();
    }
    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);
    }
    private void SdkInitializationCompletedEvent() { }

    public void ShowReward()
    {
        //IronSource.Agent.init(YOUR_APP_KEY, IronSourceAdUnits.REWARDED_VIDEO);
        IronSource.Agent.showRewardedVideo();
    }

    public void ShowBanner()
    {
        //IronSource.Agent.init(YOUR_APP_KEY, IronSourceAdUnits.BANNER);

    }
    public void Loadbanner()
    {
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
    }
    public void LoadInterstitial()
    {
        IronSource.Agent.loadInterstitial();
    }
    private IEnumerator WaitLoadInterstitial()
    {
        yield return new WaitForSeconds(1.5f);
        LoadInterstitial();
    }
    public void ShowInterstitial()
    {
        //IronSource.Agent.init(YOUR_APP_KEY, IronSourceAdUnits.INTERSTITIAL);
        IronSource.Agent.showInterstitial();
        StartCoroutine(WaitLoadInterstitial());
    }
    private IEnumerator WaitLoadInit()
    {
        m_obj.SetActive(true);
        yield return new WaitForSeconds(2f);
        m_obj.SetActive(false);
    }
}
